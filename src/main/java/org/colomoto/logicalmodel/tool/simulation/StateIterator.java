package org.colomoto.logicalmodel.tool.simulation;

import java.util.Iterator;

public interface StateIterator extends Iterator<byte[]> {
	
    public void continueOnThisNode ();
    public byte[] parent();
}
