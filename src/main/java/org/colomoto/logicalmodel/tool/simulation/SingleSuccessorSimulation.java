package org.colomoto.logicalmodel.tool.simulation;

import java.util.Stack;
import java.util.EmptyStackException;

/**
 * A simple simulation engine for updaters with a single successor.
 * It will stop when reaching a stable state, but has otherwise no memory and will
 * not detect cycles, just stop after reaching a limit on the number of iterations.
 *
 * @author Aurelien Naldi
 */
public class SingleSuccessorSimulation {

    private final DeterministicUpdater updater;
    private final byte[] init;

    private final int max_steps;

    public SingleSuccessorSimulation(DeterministicUpdater updater, byte[] init, int max_steps) {
        this.updater = updater;
        this.init = init;
        this.max_steps = max_steps;
    }

    public StateIterator iterator() {
        return new StateIteratorSync(init, updater, max_steps);
    }
}

class StateIteratorSync extends StateIteratorImpl {

    private final DeterministicUpdater updater;

    @Override
    public void continueOnThisNode () {
    	super.continueOnThisNode ();
    	queue.push (updater.getSuccessor(state));

    }

    public StateIteratorSync(byte[] state, DeterministicUpdater updater, int max_steps) {
    	super(state, max_steps, SimulationStrategy.DEPTH_FIRST);
        this.updater = updater;
    }
}
