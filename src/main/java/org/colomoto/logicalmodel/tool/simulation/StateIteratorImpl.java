package org.colomoto.logicalmodel.tool.simulation;

import java.util.EmptyStackException;
import java.util.Stack;

public abstract class StateIteratorImpl implements StateIterator {
    protected byte[] state;
    private int steps;
    
	protected Stack<byte[]> queue; // exploration queue
    
	protected Stack<byte[]> ancestors; // parents queue
	
	protected SimulationStrategy strategy;
	
	byte[] ancestor; // direct ancestor
    
    public void continueOnThisNode () {
    	ancestors.push(ancestor);
    	ancestor = state;

    	queue.push (null);
    }

    public StateIteratorImpl(byte[] state, int max_steps, SimulationStrategy strategy) {
        this.state = state;
        this.steps = max_steps;
        this.strategy = strategy;
        ancestor = null;
        
        queue = new Stack<byte[]>();
        ancestors = new Stack<byte[]>();
        queue.push(state);
    }
    
    @Override
    public byte[] parent() {
    	return ancestor;
    }

    @Override
    public boolean hasNext() {
        return state != null;
    }
    
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
    
    


    @Override
    public byte[] next() {
    	try {
    		state = queue.pop ();
    		if (state == null) {
    			ancestor = ancestors.pop ();
    			state = next();
    		}
    	} catch (EmptyStackException e) {
    		state = null;
    	}
    	return state;
    }
}
